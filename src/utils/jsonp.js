const CallbackRegistry = 'CallbackRegistry';

function getCbUrl(url, cbName) {
  const symb = url.indexOf('?') ? '&' : '?';

  return `${url}${symb}callback=${CallbackRegistry}.${cbName}`;
}

export default (url) => new Promise((resolve, reject) => {
  let done = false;
  const callbackName = `cb${String(Math.random()).slice(-6)}`;
  const script = document.createElement('script');
  script.src = getCbUrl(url, callbackName);

  if (!window[CallbackRegistry]) window[CallbackRegistry] = {};
  window[CallbackRegistry][callbackName] = (data) => {
    done = true;
    delete window[CallbackRegistry][callbackName];
    script.remove();
    resolve(data);
  };

  const scriptCb = () => {
    if (done) return;
    delete window.CallbackRegistry[callbackName];
    script.remove();
    reject(new Error('something went wrong...'));
  };
  script.onload = scriptCb;
  script.onerror = scriptCb;

  document.head.appendChild(script);
});
