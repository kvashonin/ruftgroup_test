// eslint-disable-next-line import/prefer-default-export
export function hash2obj(hash) {
  let resultHash = hash;
  if (!resultHash || (typeof resultHash !== 'string')) return {};
  if (resultHash[0] === '#') resultHash = resultHash.slice(1);

  return resultHash
    .split('&')
    .map((v) => v.split('='))
    .reduce((pre, [key, value]) => ({ ...pre, [key]: value }), {});
}

export function calculateAge(birthday) {
  const ageDifMs = Date.now() - birthday.getTime();
  const ageDate = new Date(ageDifMs);
  return Math.abs(ageDate.getUTCFullYear() - 1970);
}
