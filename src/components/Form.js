import Base from './Base';

class Form extends Base {
  constructor(query) {
    super(query);

    this.submitHooks = [];

    this.el.addEventListener('submit', (evt) => {
      evt.preventDefault();

      this.submitHooks.forEach((submitHook) => submitHook(evt));
    });
  }

  submit(cb) {
    this.submitHooks.push(cb);
  }
}

export default Form;
