import Base from './Base';

class Error extends Base {
  show(text) {
    this.el.classList.add('error_show');
    this.el.innerText = text;
  }

  hide() {
    this.el.classList.remove('error_show');
  }
}

export default Error;
